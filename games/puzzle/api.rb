module Games
  class PuzzleApi < Sinatra::Base
    @x = 0          # X coordinate where 0 will be located
    @y = 0          # Y coordinate where 0 will be located
    @game = nil     # Document of mongo
    @pid = nil      # participation ID
    @m = nil        # Matrix suffle
    @mSorted = nil  # Matrix sorted
    @error = nil    # Error in the game
    @win = false    # State of game

    def createMatrix(size)
      limit = size*size
      numbersOrdered = (0..limit - 1).to_a
      numbers = (0..limit - 1).to_a.shuffle
      @m = Array.new(size) { Array.new(size, 0) }
      @mSorted = Array.new(size) { Array.new(size, 0) }

      @m.each_with_index do |e, row|
        e.each_with_index do |f, col|
          @m[row][col] = numbers.pop
          @mSorted[row][col] = numbersOrdered.pop
          if @m[row][col] == 0
            @x = row
            @y = col
          end
        end
      end

      self.storeNewPuzzle(size)
    end

    def storeNewPuzzle(size)
      begin
        @game = Puzzle.new(
          pid: @pid,
          matrix: @m,
          matrixSorted: @mSorted,
          positionZeroX: @x,
          positionZeroY: @y,
          size: size
        )
        @game.save
        self.createResponse()
      rescue => exception
        puts "Ha ocurrido un error al almacenar"
      end
    end

    def createResponse
      mSuffle = "\n"
      mSorted  = "\n"

      @game.matrix.each_with_index do |e, row, col|
        e.each_with_index do |f, col,|
          mSuffle += @game.matrix[row][col].to_s + " "
          mSorted += @game.matrixSorted[row][col].to_s + " "
        end
        mSuffle += "\n"
        mSorted += "\n"
      end


      if mSuffle == mSorted
        @win = true
      end

      Oj.dump(
        objective: mSorted,
        matrix: mSuffle,
        position: {x: @game.positionZeroX, y: @game.positionZeroY},
        size: @game.size,
        error: @error,
        win: @win
      )
    end

    def validateMovement(movement)
      case movement
      when "right"
        if(@game.positionZeroY != @game.size - 1)
          auxItem = @game.matrix[@game.positionZeroX][@game.positionZeroY+1]
          @game.matrix[@game.positionZeroX][@game.positionZeroY+1] = 0
          @game.matrix[@game.positionZeroX][@game.positionZeroY] = auxItem
          @game.positionZeroY = @game.positionZeroY + 1
          @game.save
        else
          @error = "El movimiento no se pudo realizar"
        end
      when "left"
        if(@game.positionZeroY != 0)
          auxItem = @game.matrix[@game.positionZeroX][@game.positionZeroY-1]
          @game.matrix[@game.positionZeroX][@game.positionZeroY-1] = 0
          @game.matrix[@game.positionZeroX][@game.positionZeroY] = auxItem
          @game.positionZeroY = @game.positionZeroY - 1
          @game.save
        else
          @error = "El movimiento no se pudo realizar"
        end
      when "down"
        if(@game.positionZeroX != @game.size - 1)
          auxItem = @game.matrix[@game.positionZeroX+1][@game.positionZeroY]
          @game.matrix[@game.positionZeroX+1][@game.positionZeroY] = 0
          @game.matrix[@game.positionZeroX][@game.positionZeroY] = auxItem
          @game.positionZeroX = @game.positionZeroX + 1
          @game.save
        else
          @error = "El movimiento no se pudo realizar"
        end
      when "up"
        if(@game.positionZeroX != 0)
          auxItem = @game.matrix[@game.positionZeroX-1][@game.positionZeroY]
          @game.matrix[@game.positionZeroX-1][@game.positionZeroY] = 0
          @game.matrix[@game.positionZeroX][@game.positionZeroY] = auxItem
          @game.positionZeroX = @game.positionZeroX - 1
          @game.save
        else
          @error = "El movimiento no se pudo realizar"
        end
      else
        @error = "Se ha recibido un movimiento que no coincide, con los permitidos."
      end
      self.createResponse()
    end

    get '/puzzle/:pid/:size' do
      @game = Puzzle.where(pid: params[:pid]).first
      @pid = params[:pid]
      size = params[:size].to_i
      if @game.nil?
        self.createMatrix(size)
      end
    end

    get '/puzzle/:pid/play/:movement' do
      @game = Puzzle.where(pid: params[:pid]).first
      @error = nil
      if @game.nil?
        Oj.dump(
          error: "No existe una partida en juego."
        )
      else
        self.validateMovement(params[:movement])
      end
    end

  end
end