module Games
  class Puzzle
    include Mongoid::Document

    field :matrix, type: Array, default: []
    field :matrixSorted, type: Array, default: []
    field :positionZeroX, type: Integer
    field :positionZeroY, type: Integer
    field :size, type: Integer
    field :pid, type: String
    index({ pid: 1 }, unique: true)
  end
end