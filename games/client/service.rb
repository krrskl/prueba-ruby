require 'json'
require 'net/http'


module GameClient
  class Service
    def initialize(api_url, port)
      @api_url = api_url
      @port = port
      @http = Net::HTTP
    end
  
    def initializeGame(pid)
      begin
        response = @http.get_response(@api_url, '/hangman/' + pid, @port)
  
        return JSON.parse(response.body)
      rescue => exception
        puts "Ha ocurrido un error al comunicarnos con el servidor."
        puts exception
        exit
      end
    end
  
    def sendLetter(pid, letter)
      begin
        response = @http.get_response(@api_url, '/hangman/' + pid + '/try/' + letter, @port)
  
        return JSON.parse(response.body)
        
      rescue => exception
        puts "Ha ocurrido un error al comunicarnos con el servidor."
        exit
      end
    end
  end
end