require_relative 'service'
require 'json'

module GameClient
  class Game
    def initialize
      @api = 'localhost'
      @port = 9292
      @pid = nil
      @state = nil
      @game = GameClient::Service.new(@api, @port)
    end
  
    def setUser
      @pid = gets.chomp
      response = @game.initializeGame(@pid)
      self.printHint(response)
    end
  
    def printHint(res)
      puts "\n\n"
      puts res[":hint"]
      puts "\n\n"
    end
  
    def getLetter
      letter = gets.chomp
      response = @game.sendLetter(@pid, letter)
      
      puts "\n\n\n\n"  
      puts JSON.pretty_generate(response)
  
      @state = response[":state"]

      if @state == "win"
        puts "Ganaste !!"
      elsif @state != "playing"
        puts "Perdiste !!"
      end

      self.next()
    end
  
    def next
      while @state == "playing"
        puts "\n\n\n Ingrese una letra" 
        self.getLetter()
      end
    end
  end
end