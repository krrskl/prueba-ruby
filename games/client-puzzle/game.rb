require_relative 'service'
require 'json'

module GameClientPuzzle
  class Game
    def initialize
      @api = 'localhost'
      @port = 9292
      @pid = nil
      @win = nil
      @game = GameClientPuzzle::Service.new(@api, @port)
    end
  
    def setUser
      @pid = gets.chomp
      puts "Indique el tamaño de la matrix NxN"
      size = gets.chomp
      response = @game.initializeGame(@pid, size)
      self.printMatrix(response)
    end
  
    def printMatrix(res)
      puts "\n\n Estado actual"
      puts res[":matrix"]
      puts "\n\n Objetivo"
      puts res[":objective"]
      puts "\n"
      if res[":error"] != "null"
        puts "☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰ \n"
        puts res[":error"]
        puts "☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰ \n"
      end

      if @win == true
        puts "Ganaste !!"
      end

      self.next()
    end
  
    def getMovement
      movement = gets.chomp
      response = @game.sendMovement(@pid, movement)
      
      puts "\n\n"
      
      @win = response[":win"]
      self.printMatrix(response)
    end
  
    def next
      while @win == nil or @win == "null"
        puts "\n\n\n Indique un movimiento"
        self.getMovement()
      end
    end
  end
end