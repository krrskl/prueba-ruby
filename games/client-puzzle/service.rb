require 'json'
require 'net/http'


module GameClientPuzzle
  class Service
    def initialize(api_url, port)
      @api_url = api_url
      @port = port
      @http = Net::HTTP
    end
  
    def initializeGame(pid, size)
      begin
        response = @http.get_response(@api_url, '/puzzle/' + pid + '/' + size, @port)
  
        return JSON.parse(response.body)
      rescue => exception
        puts "Ha ocurrido un error al comunicarnos con el servidor."
        exit
      end
    end
  
    def sendMovement(pid, movement)
      begin
        response = @http.get_response(@api_url, '/puzzle/' + pid + '/play/' + movement, @port)
  
        return JSON.parse(response.body)
        
      rescue => exception
        puts "Ha ocurrido un error al comunicarnos con el servidor."
        exit
      end
    end
  end
end