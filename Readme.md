# Prueba Backend junior

> Acontinuación se indican las soluciones aplicadas para resolver los retos planteados en la prueba.

## Corriendo el servidor
> Para usar las API y los clientes es necesario tener el servidor corriendo por lo que deberémos correr los siguientes pasos y comandos

### Importante tener instalado [MongoDB](https://www.mongodb.com/es)

```bash

bundle install

ruby bin/setup.rb

rackup

```

## Reto #1

La solución de este reto se puede vizualizar en el archivo  ``` api.rb ``` ubicado en la ruta:
``` /games/hangman/api.rb ``` desde la línea [68](https://bitbucket.org/krrskl/prueba-ruby/src/05d2bc26b61d5d8e9afee61057eccb8ba6edae24/games/hangman/api.rb#lines-68) a la 76

---

## Reto #2

Para este reto he creado una carpeta llamada ``` client ``` y la he ubicado en ``` /games/client ``` dentro de esta carpeta he creado 3 archivos:

```

├── client
|   ├── game.rb      # Encargado de implementar las acciones dadas por el usuario en el terminal.
|   ├── main.rb      # Encargado de crear una instancia del juego.
|   ├── service.rb   # Encargado de consumir la API
|

```

Para hacer el uso de este cliente deberá navegar a través de la terminal hasta ``` /games/client ``` y luego usando el siguiente comando:
```sh 
ruby ./main.rb
```
---
## Reto #3

Este reto ha sido desarrollado en la carpeta ``` puzzle ``` ubicado en ``` /games/puzzle ``` dentro de esta carpeta he creado 2 archivos:

```

├── puzzle
|   ├── api.rb      # Contiene las rutas y funciones que el juego usará.
|   ├── puzzle.rb      # Contiene los atributos a manejar con mongoDB.
|

```

Las rutas que contiene son las siguientes: 

```
/puzzle/:pid/:size
```
> Parámetros

```
:pid  # Id de la partida
:size # Tamaño para generar la matriz NxN
```
---

```
/puzzle/:pid/play/:movement
```

> Parámetros
```
:pid      # Id de la partida
:movement # Movimiento que deberá realizarce en la matriz
```

A su vez he decidido crear un cliente para la terminal y lo he situado en ``` client-puzzle ``` ubicado en ``` /games/client-puzzle ``` dentro de esta carpeta he creado 3 archivos:

```

├── client
|   ├── game.rb      # Encargado de implementar las acciones dadas por el usuario en el terminal.
|   ├── main.rb      # Encargado de crear una instancia del juego.
|   ├── service.rb   # Encargado de consumir la API
|

```

Para hacer el uso de este cliente deberá navegar a través de la terminal hasta ``` /games/client ``` y luego usando el siguiente comando:
```sh 
ruby ./main.rb
```

los movimientos que acepta son:

1. up
2. down
3. left
4. right

___
Hecho con ❤️ por [Rubén Carrascal](https://krrskl.github.io/)
